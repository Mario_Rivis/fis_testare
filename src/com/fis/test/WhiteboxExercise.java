package com.fis.test;

public class WhiteboxExercise {

    private long a;
    private long b;
    private long c;

    public WhiteboxExercise(long a, long b, long c) {
        this.a = a;
        this.b = b;
        this.c = c;
    }

    public long doSomeStuff() throws SomeException {
        if (a > 0) {
            if (b < 3)
                if (c < -100)
                    return a * b - c;
                else
                    throw new SomeException("C is to big!");
            else
                return a - b + c;
        } else {
            if (b > 5)
                if (c <= 20)
                    return 2 * a + 4 * b - 5 * c;
                else
                    throw new SomeException();
            else return b + 2;
        }
    }

    public class SomeException extends Exception {
        public SomeException(){}
        public SomeException(String s) {
            super(s);
        }
    }
}
